"""
executeDT.py
takes a file defining a decision tree ensemble and a data set, and produces
(a)(standard output)
The number of classification errors, recognition rate (% correct)
and profit obtained.
(b)(standard output)
A confusion matrix that gives a histogram of the output class
(rows) vs. correct class (columns) results.
(c)(image file)
containing a plot of the classification regions
. The program runs the current network over a grid of equally spaced 
points in the region (0,0) to (1,1) (the limits of the feature space), 
using a different color to represent the classification
decision at each point. Use matplotlib to produce this image.
"""

import sys
import pickle
from trainDT import Tree, Node
import matplotlib.pyplot as pyplot
import numpy as np

"""
Prints the confusion matrix to stdout
"""
def printConfusionMatrix(confMatrix):
    print '%20s %30s' % ('', 'Actual class')
    print '%20s %10s %10s %10s %10s' % ('Assigned Class', 'bolt', 'nut', 'ring', 'scrap')
    print '%20s %10d %10d %10d %10d' % (
        'bolt', confMatrix[0][0], confMatrix[0][1], confMatrix[0][2], confMatrix[0][3] )
    print '%20s %10d %10d %10d %10d' % (
        'nut', confMatrix[1][0], confMatrix[1][1], confMatrix[1][2], confMatrix[1][3] )
    print '%20s %10d %10d %10d %10d' % (
        'ring', confMatrix[2][0], confMatrix[2][1], confMatrix[2][2], confMatrix[2][3] )
    print '%20s %10d %10d %10d %10d' % (
        'scrap', confMatrix[3][0], confMatrix[3][1], confMatrix[3][2], confMatrix[3][3] )

"""
Reads the file of samples
"""
def readSampleFile(filename):
    samples = []
    file = open(filename)
    for line in file:
        instance = line.split(',')
        if len(instance) == 3:
            samples.append([])
            samples[-1] = [float(instance[0]), float(instance[1]), int(instance[2])]
    return samples

"""
The main function which handles high level program logic.
Makes a confusion matrix, a decision boundary graph, and totals the profit made
"""
def main():
    samples = readSampleFile(sys.argv[2])
    f = open(sys.argv[1])
    ensemble = pickle.load(f)

    profitMat = [[0.20, -0.07, -0.07, -0.07],
                 [-0.07, 0.15, -0.07, -0.07],
                 [-0.07, -0.07, 0.05, -0.07],
                 [-0.03, -0.03, -0.03, -0.03]]
    confusionMat = [[0.00, 0.00, 0.00, 0.00], [0.00, 0.00, 0.00, 0.00], [0.00, 0.00, 0.00, 0.00],
                    [0.00, 0.00, 0.00, 0.00]]

    correct = 0.0
    incorect = 0.0
    profit = 0.00

    #classify samples
    for sample in samples:
        d = {1: 0.00, 2: 0.00, 3: 0.00, 4: 0.00}
        for tree in ensemble:
            ans = tree.classify(sample)
            for b in d.keys():
                d[b] += ans[b]
        classification = max(d, key=d.get)
        if classification != sample[2]:
            incorect += 1.00
        else:
            correct += 1.00
        profit += profitMat[classification - 1][sample[2] - 1]
        confusionMat[classification - 1][sample[2] - 1] += 1.00

    #build classification boundary graph
    redx, bluex, greenx, yellowx = [], [], [], []
    redy, bluey, greeny, yellowy = [], [], [], []
    for xc in np.arange(0, 1, 0.006):
        for yc in np.arange(0, 1, 0.006):
            sample = [xc, yc]
            d = {1: 0.00, 2: 0.00, 3: 0.00, 4: 0.00}
            for tree in ensemble:
                ans = tree.classify(sample)
                for b in d.keys():
                    d[b] += ans[b]
            classification = max(d, key=d.get)
            if classification == 4:
                redx.append(xc)
                redy.append(yc)
            if classification == 1:
                bluex.append(xc)
                bluey.append(yc)
            if classification == 3:
                greenx.append(xc)
                greeny.append(yc)
            if classification == 2:
                yellowx.append(xc)
                yellowy.append(yc)
    pyplot.plot(redx, redy, 'ro', bluex, bluey, 'bo', greenx, greeny, 'go', yellowx, yellowy, 'yo')
    pyplot.xlabel("Six-fold rotational symmetry")
    pyplot.ylabel("Eccentricity")
    pyplot.title("No. of trees in ensemble = " + str(len(ensemble)))
    pyplot.show()

    #find accuracy
    accuracy = correct / (correct + incorect)
    col_totals = [sum(x) for x in zip(*confusionMat)]

    print "[" + sys.argv[2] + "]"
    print ""
    print "No. of correctly classified samples: " + str(int(correct))
    print "No. of incorrectly classified samples: " + str(int(incorect))
    print "Recognition rate: " + str(accuracy * 100.00) + " %"
    print "Profit obtained: " + str(profit)
    print "Confusion Matrix:"

    printConfusionMatrix(confusionMat)

    print "________________________________________________________________"


main()

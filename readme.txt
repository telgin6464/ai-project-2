To run the MLP training program, use the following command:
python trainMLP.py <sample csv file>

This program will write five files:
weightsMLP_0.txt
weightsMLP_10.txt
weightsMLP_100.txt
weightsMLP_1000.txt
weightsMLP_10000.txt

where each one contains a neural network representation after having gone through
the specified number of iterations.

The program will also display a plot of the sum of squared errors at each iteration.


To run the MLP evaluation program, use the following command:
python executeMLP.py <sample csv file> <weight file>
where the weight file is a file written by trainMLP.py

This program will output the percent of correctly classified samples,
the profit made, and a confusion matrix of the classifications.

This program will also display a plot showing the decision boundaries of the network
by coloring samples which are uniformly spaced across the grid according to their
evaluated classifications.

To run the DT training program, use the following command:
python trainDT.py <sample csv file>

This program will write four files:
train1
train10
train100
train200

where each one contains the random forest ensemble trained with the specified number of trees. 

The program will also display a plot of the sum of squared errors after having 0 to 200 trees.


To run the DT evaluation program, use the following command:
python executeDT.py <random forest file> <sample csv file>

where the random forest file is a file written by trainDT.py

This program will output the percent of correctly classified samples,
the profit made, and a confusion matrix of the classifications.

This program will also display a plot showing the decision boundaries of the ensemble
by coloring samples which are uniformly spaced across the grid according to their
evaluated classifications.
"""
@Author Thomas Elgin (twe9697)
"""

import math
import sys
import random
import matplotlib.pyplot as pyplot

"""
Reads the samples stored in the specified csv and returns
a list of those samples paired with their classifications.
"""
def readSampleFile(filename):
    samples = []
    inFile = open(filename)

    for line in inFile:
        if line.count(",") == 2:
            parts = line.split(",")
            samples += [((float(parts[0]), float(parts[1])), int(parts[2]))]

    return samples;

"""
Writes the weights to a file, so that the network can be reconstructed
"""
def writeWeightFile(filename, inputNeurons, inputHiddenWeights, hiddenNeurons, hiddenOutputWeights, outputNeurons):
    outFile = open(filename, 'w')
    print "Writing File: " + filename
    outFile.write(str(len(inputNeurons)-1) + "," + str(len(hiddenNeurons)-1) + "," + str(len(outputNeurons)) + "\n")
    for weightLine in inputHiddenWeights:
        line = ""
        for weight in weightLine:
            line += str(weight) + ","
        outFile.write(line[:-1] + "\n")

    for weightLine in hiddenOutputWeights:
        line = ""
        for weight in weightLine:
            line += str(weight) + ","
        outFile.write(line[:-1] + "\n")

    outFile.close()

"""
Updates one specific weight
"""
def updateWeight(w, n, vFrom, dTo):
    answer = w - (n * dTo * vFrom)

    return answer


"""
Computes delta of an output layer neuron
"""
def deltaOut(v, target):
    answer = (v - target) * v * (1 - v)

    return answer


"""
Computes delta of a hidden or input layer neuron
"""
def deltaHidden(v, deltaWeightPairs):
    sum = 0
    for p in deltaWeightPairs:
        sum += (p[0] * p[1])

    answer = sum * v * (1 - v)

    return answer


"""
Returns the value of the sigmoid function with input value x
"""
def sigmoid(x):
    return 1.0 / (1.0 + math.pow(math.e, -x))


"""
Computes deltas of output and hidden layer neurons, then updates all the weights
"""
def update(alpha, sample, inputNeurons, inputHiddenWeights, hiddenNeurons, hiddenOutputWeights, outputNeurons):
    outputDeltas = []
    hiddenDeltas = [None]
    target = getNetworkOutput(sample)

    # compute output layer deltas
    for n in range(len(outputNeurons)):
        outputDeltas += [deltaOut(outputNeurons[n], target[n])]

    #compute hidden layer deltas
    for n in range(1, len(hiddenNeurons)):
        deltaWeightPairs = []
        for i in range(len(outputNeurons)):
            deltaWeightPairs += [[outputDeltas[i], hiddenOutputWeights[n][i]]]
        hiddenDeltas += [deltaHidden(hiddenNeurons[n], deltaWeightPairs)]

    #compute new input -> hidden weights
    for f in range(len(inputHiddenWeights)):
        for t in range(1, len(hiddenNeurons)):
            inputHiddenWeights[f][t] = updateWeight(inputHiddenWeights[f][t], alpha, inputNeurons[f], hiddenDeltas[t])

    #compute new hidden -> output weights
    for f in range(len(hiddenOutputWeights)):
        for t in range(len(outputNeurons)):
            hiddenOutputWeights[f][t] = updateWeight(hiddenOutputWeights[f][t], alpha, hiddenNeurons[f], outputDeltas[t])

    #compute error:
    error = 0
    for n in range(len(outputDeltas)):
        error += math.pow(outputDeltas[n],2)
    for n in range(1,len(hiddenDeltas)):
        error += math.pow(hiddenDeltas[n],2)

    return error

"""
Converts a sample's class number into the binary classification format
"""
def getNetworkOutput(sample):
    sampleClass = sample[1]
    if sampleClass == 1:
        return [1, 0, 0, 0]
    elif sampleClass == 2:
        return [0, 1, 0, 0]
    elif sampleClass == 3:
        return [0, 0, 1, 0]
    elif sampleClass == 4:
        return [0, 0, 0, 1]
    else:
        print "Unknown sample class: " + str(sampleClass)


"""
Connection class representing directed edges
"""
def evaluate(sample, inputNeurons, inputHiddenWeights, hiddenNeurons, hiddenOutputWeights, outputNeurons):
    #get input layer activations
    for n in range(1, len(inputNeurons)):
        inputNeurons[n] = sample[0][n-1]

    #get hidden layer activations
    for n in range(1, len(hiddenNeurons)):
        sum = 0
        for f in range(len(inputNeurons)):
            sum += (inputHiddenWeights[f][n] * inputNeurons[f])
        hiddenNeurons[n] = sigmoid(sum)

    #get output layer activations
    for n in range(len(outputNeurons)):
        sum = 0
        for f in range(len(hiddenNeurons)):
            sum += (hiddenOutputWeights[f][n] * hiddenNeurons[f])
        outputNeurons[n] = sigmoid(sum)

    #find highest value in output layer
    maxValue = outputNeurons[0]
    maxNumber = 0
    for n in range(1, len(outputNeurons)):
        if outputNeurons[n] > maxValue:
            maxValue = outputNeurons[n]
            maxNumber = n

    #find the classification (highest value in 0 --> class 1)
    classification = maxNumber + 1

    return classification


"""
Plots the error of the network against the number of training iterations
"""
def plotError(errors):
    pyplot.plot([i for i in range(len(errors))], errors, 'r-')
    pyplot.axis([len(errors)*-.01, len(errors), 0.0, errors[0]*1.1])
    pyplot.xlabel("# iterations")
    pyplot.ylabel("Summed Squared Error");
    pyplot.show()

"""
The main function that defines the high level program logic
"""
def main():
    # config constants
    ALPHA = .1
    INPUT_NEURON_COUNT = 2
    HIDDEN_NEURON_COUNT = 5
    OUTPUT_NEURON_COUNT = 4
    ITERATIONS = 10000
    SEED = 2

    #build neuron vectors
    inputNeurons = [1] + ([None] * INPUT_NEURON_COUNT)
    hiddenNeurons = [1] + ([None] * HIDDEN_NEURON_COUNT)
    outputNeurons = [None] * OUTPUT_NEURON_COUNT


    #build weight matrices
    random.seed(SEED)
    inputHiddenWeights = []
    for f in range(len(inputNeurons)):
        lst = []
        #note: creates weights to hidden layer bias neuron just so indices match up
        for t in range(len(hiddenNeurons)):
            lst += [(random.random()*2)-1]
        inputHiddenWeights += [lst]

    hiddenOutputWeights = []
    for f in range(len(hiddenNeurons)):
        lst = []
        for t in range(len(outputNeurons)):
            lst += [(random.random()*2)-1]
        hiddenOutputWeights += [lst]

    samples = readSampleFile(sys.argv[1])

    writeWeightFile("weightsMLP_0.txt", inputNeurons, inputHiddenWeights, hiddenNeurons, hiddenOutputWeights, outputNeurons)

    #train network
    errors = []
    for i in range(ITERATIONS):
       
        #evaluate and update
        error = 0
        for sample in samples:
            evaluate(sample, inputNeurons, inputHiddenWeights, hiddenNeurons, hiddenOutputWeights, outputNeurons)
            error += update(ALPHA, sample, inputNeurons, inputHiddenWeights, hiddenNeurons, hiddenOutputWeights, outputNeurons)

        #save weight files
        if i == 10-1:
            writeWeightFile("weightsMLP_10.txt", inputNeurons, inputHiddenWeights, hiddenNeurons, hiddenOutputWeights, outputNeurons)
        elif i == 100-1:
            writeWeightFile("weightsMLP_100.txt", inputNeurons, inputHiddenWeights, hiddenNeurons, hiddenOutputWeights, outputNeurons)
        elif i == 1000-1:
            writeWeightFile("weightsMLP_1000.txt", inputNeurons, inputHiddenWeights, hiddenNeurons, hiddenOutputWeights, outputNeurons)
        elif i == 10000-1:
            writeWeightFile("weightsMLP_10000.txt", inputNeurons, inputHiddenWeights, hiddenNeurons, hiddenOutputWeights, outputNeurons)

        #keep this epoch's error
        errors += [error]

    #plots the error of the network after each iteration
    plotError(errors)

main();

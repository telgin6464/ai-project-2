"""
trainDT.py
takes a training samples file and produces:
(a) Four files, for the randomForest after 1, 10, 100, and a maximum (e.g. 200)
 number of trees have been added (for use with executeDT.py).
(b) A learning curve plot, showing the SSE between the classifier outputs
 and true classes after each classifier is added to the randomForest, showing
the sum of squared differences between the weighted vote output of the 
randomForest vs. the true class.
"""

import sys
import math
import random
import pickle
import matplotlib.pyplot as pyplot

"""
The node class stores info about the DT nodes
"""
class Node:
    def __init__(self):
        self.left = None
        self.right = None
        self.split = None
        self.splitVal = None
        self.isLeaf = False
        self.probDist = {1: 0.00, 2: 0.00, 3: 0.00, 4: 0.00}

    def setLeft(self, left):
        self.left = left

    def setRight(self, right):
        self.right = right

    def setSplit(self, split):
        self.split = split

    def setSplitVal(self, splitval):
        self.splitVal = splitval

    def makeLeaf(self, prob):
        self.isLeaf = True
        self.probDist = prob

"""
The tree class stores information about a tree
"""
class Tree:
    # constructor
    def __init__(self):
        self.root = Node()

    #
    def classify(self, sample):
        node = self.root
        while not node.isLeaf:
            if sample[node.split] > node.splitVal:
                node = node.right
            else:
                node = node.left
        return node.probDist


"""
Computes the entropy of the samples
"""
def entropy(samples):
    ent = 0.00
    p = {1: 0.00, 2: 0.00, 3: 0.00, 4: 0.00}
    for sample in samples:
        p[sample[2]] += 1
    for x in p:
        prob = float(p[x]) / len(samples)
        log = 0.00
        if prob > 0.00:
            log = math.log(prob, 2)
        ent += prob * -log
    return ent

"""
Calculates the information gained from a split
"""
def infoGain(l, r):
    igl = entropy(l)
    igr = entropy(r)
    igt = entropy(l + r)
    ig = igt - ((float(len(l)) * igl + float(len(r)) * igr) / float(len(l) + len(r)))
    return ig

"""
Trains a DT of the specified depth
"""
def trainTree(current, samples, currentDepth, depthLimit):
    if len(samples) == 0:
        x = random.randint(1, 4)
        current.probDist[x] = 1.00
        current.isLeaf = True
        return

    count = {1: 0, 2: 0, 3: 0, 4: 0}
    for x in samples:
        count[x[2]] += 1

    for x in count.keys():
        if count[x] == len(samples):
            current.probDist[x] = 1.00
            current.isLeaf = True
            return

    if currentDepth == depthLimit:
        n = float(len(samples))
        for x in samples:
            current.probDist[x[2]] += 1.00

        for x in current.probDist.keys():
            current.probDist[x] /= n

        current.isLeaf = True
        return

    sorted1 = sorted(samples, key=lambda samples: samples[0])
    sorted2 = sorted(samples, key=lambda samples: samples[1])
    flag = 0
    igmax = -1.00
    bestSplit = 0.00

    for a in range(len(samples) - 1):

        left, right = sorted1[0:a + 1], sorted1[a + 1:len(samples)]
        ig = infoGain(left, right)
        if ig > igmax:
            igmax = ig
            bestSplit = (sorted1[a][0] + sorted1[a + 1][0]) / 2.00
            maxleft = left
            maxright = right

    for a in range(len(samples) - 1):
        left, right = sorted2[0:a + 1], sorted2[a + 1:len(samples)]
        ig = infoGain(left, right)
        if ig > igmax:
            flag = 1
            igmax = ig
            bestSplit = ( sorted2[a][0] + sorted2[a + 1][0]) / 2.00
            maxleft = left
            maxright = right

    current.left = Node()
    current.right = Node()
    current.setSplit(flag)
    current.setSplitVal(bestSplit)

    trainTree(current.left, maxleft, currentDepth + 1, depthLimit)
    trainTree(current.right, maxright, currentDepth + 1, depthLimit)

"""
Calculates the sum of squared errors
"""
def sse(y, yprime):
    a = 0.00
    for i in range(len(y)):
        b = 0.00
        for j in y[i].keys():
            b += (y[i][j] - yprime[i][j]) ** 2.00
        a += b
    return a / float(len(y))


"""
Generates a random forest by adding an additional tree for each iteration
"""
def generateRandomForest(randomForest, error, samples, iterations, randomness, limit):
    k = int(len(samples) * randomness)

    y = []
    for sample in samples:
        v1 = {1: 0.00, 2: 0.00, 3: 0.00, 4: 0.00}
        v1[sample[2]] = 1.00
        y.append(v1)

    for n in range(iterations):
        dt = Tree()
        trainTree(dt.root, random.sample(samples, k), 0, limit)
        randomForest.append(dt)
        norm = float(len(randomForest))
        c, i = 0.00, 0.00
        yprime = []
        for sample in samples:
            vote = {1: 0.00, 2: 0.00, 3: 0.00, 4: 0.00}
            for tree in randomForest:
                latestClass = tree.classify(sample)
                for key in latestClass.keys():
                    vote[key] += latestClass[key]

            for key in vote.keys():
                vote[key] = vote[key] / norm

            result = max(vote, key=vote.get)
            yprime.append(vote)

            if result == sample[2]:
                c += 1.00
            else:
                i += 1.00

        error.append(sse(yprime, y))

    return randomForest, error

"""
Makes a graph of the SSE
"""
def mkErrorGraph(error):
    pyplot.plot(error)
    pyplot.ylabel('SSE')
    pyplot.xlabel('randomForest size')
    pyplot.show()

"""
Writes a tree file
"""
def writeTreeFile(randomForest, treeCount):
    name = 'tree' + str(treeCount)
    f = open(name, 'w')
    print "writing:", name
    pickle.dump(randomForest, f)

"""
Reads the samples from a file
"""
def readSampleFile(filename):
    samples = []
    file = open(filename)
    for line in file:
        instance = line.split(',')
        if len(instance) == 3:
            samples.append([])
            samples[-1] = [float(instance[0]), float(instance[1]), int(instance[2])]
    return samples

"""
The main function which handles high level program logic
"""
if __name__ == '__main__':
    samples = readSampleFile(sys.argv[1])
    error = []
    randomForest = []
    randomForest, error = generateRandomForest(randomForest, error, samples, 1, 0.2, 2)
    writeTreeFile(randomForest, 1)
    randomForest, error = generateRandomForest(randomForest, error, samples, 9, 0.2, 2)
    writeTreeFile(randomForest, 10)
    randomForest, error = generateRandomForest(randomForest, error, samples, 90, .2, 2)
    writeTreeFile(randomForest, 100)
    randomForest, error = generateRandomForest(randomForest, error, samples, 100, .2, 2)
    writeTreeFile(randomForest, 200)

    mkErrorGraph(error)



"""
@Author Thomas Elgin (twe9697)
"""

import math
import sys
import matplotlib.pyplot as pyplot

"""
Reads the samples stored in the specified csv and returns 
a list of those samples paired with their classifications.
"""
def readSampleFile(filename):
    samples = []
    inFile = open(filename)

    for line in inFile:
        if line.count(",") == 2:
            parts = line.split(",")
            samples += [((float(parts[0]), float(parts[1])), int(parts[2]))]

    return samples;


"""
Reads the weights stored by trainMLP
"""
def readWeightFile(filename):
    # read file
    inFile = open(filename)
    lines = inFile.readlines()

    #setup network sizes
    parts = lines[0].split(",")
    inputNeurons = [1] + ([None] * int(parts[0]))
    hiddenNeurons = [1] + ([None] * int(parts[1]))
    outputNeurons = ([None] * int(parts[2]))

    #setup weight matrices
    inputHiddenWeights = []
    for n in range(1, len(inputNeurons) + 1):
        inputHiddenWeights += [[float(i) for i in lines[n].split(",")]]

    hiddenOutputWeights = []
    for n in range(len(inputNeurons) + 1, len(inputNeurons) + 1 + len(hiddenNeurons)):
        hiddenOutputWeights += [[float(i) for i in lines[n].split(",")]]

    return inputNeurons, inputHiddenWeights, hiddenNeurons, hiddenOutputWeights, outputNeurons;


"""
Returns the value of the sigmoid function with input value x
"""
def sigmoid(x):
    return 1.0 / (1.0 + math.pow(math.e, -x))


"""
Connection class representing directed edges
"""
def evaluate(sample, inputNeurons, inputHiddenWeights, hiddenNeurons, hiddenOutputWeights, outputNeurons):
    # get input layer activations
    for n in range(1, len(inputNeurons)):
        inputNeurons[n] = sample[0][n - 1]

    #get hidden layer activations
    for n in range(1, len(hiddenNeurons)):
        sum = 0
        for f in range(len(inputNeurons)):
            sum += (inputHiddenWeights[f][n] * inputNeurons[f])
        hiddenNeurons[n] = sigmoid(sum)

    #get output layer activations
    for n in range(len(outputNeurons)):
        sum = 0
        for f in range(len(hiddenNeurons)):
            sum += (hiddenOutputWeights[f][n] * hiddenNeurons[f])
        outputNeurons[n] = sigmoid(sum)

    #find highest value in output layer
    maxValue = outputNeurons[0]
    maxNumber = 0
    for n in range(1, len(outputNeurons)):
        if outputNeurons[n] > maxValue:
            maxValue = outputNeurons[n]
            maxNumber = n

    #find the classification (highest value in 0 --> class 1)
    classification = maxNumber + 1

    return classification


"""
Plots the samples and shows their classifications and x's where the network
failed to classify a sample correctly
"""
def plotSamples(samples):
    class1f1 = []
    class1f2 = []
    class2f1 = []
    class2f2 = []
    class3f1 = []
    class3f2 = []
    class4f1 = []
    class4f2 = []

    for sample in samples:
        if sample[1] == 1:
            class1f1 += [sample[0][0]]
            class1f2 += [sample[0][1]]
        elif sample[1] == 2:
            class2f1 += [sample[0][0]]
            class2f2 += [sample[0][1]]
        elif sample[1] == 3:
            class3f1 += [sample[0][0]]
            class3f2 += [sample[0][1]]
        elif sample[1] == 4:
            class4f1 += [sample[0][0]]
            class4f2 += [sample[0][1]]
        else:
            print "unknown", sample[1]

    pyplot.clf()
    pyplot.plot(class1f1, class1f2, 'ro')
    pyplot.plot(class2f1, class2f2, 'co')
    pyplot.plot(class3f1, class3f2, 'bo')
    pyplot.plot(class4f1, class4f2, 'go')
    pyplot.axis([0.0, 1.0, 0.0, 1.0])
    pyplot.xlabel("Feature 1")
    pyplot.ylabel("Feature 2")
    pyplot.show()


"""
The main function that defines the high level program logic
"""
def main():
    # config constants
    ALPHA = .1
    GRID_SIZE = 50

    # read files, construct network
    samples = readSampleFile(sys.argv[1])
    inputNeurons, inputHiddenWeights, hiddenNeurons, hiddenOutputWeights, outputNeurons = readWeightFile(sys.argv[2])

    #build profit matrix
    profitMatrix = [[.2, -.07, -.07, -.07]]
    profitMatrix += [[-.07, .15, -.07, -.07]]
    profitMatrix += [[-.07, -.07, .05, -.07]]
    profitMatrix += [[-.03, -.03, -.03, -.03]]

    #build confusion matrix
    confusionMatrix = [[0] * len(outputNeurons)]
    confusionMatrix += [[0] * len(outputNeurons)]
    confusionMatrix += [[0] * len(outputNeurons)]
    confusionMatrix += [[0] * len(outputNeurons)]

    #evaluate each sample
    right = 0
    profit = 0
    for sample in samples:
        evaluation = evaluate(sample, inputNeurons, inputHiddenWeights, hiddenNeurons, hiddenOutputWeights, outputNeurons)
        if evaluation == sample[1]:
            right += 1
        profit += profitMatrix[evaluation-1][sample[1]-1]
        confusionMatrix[evaluation-1][sample[1]-1] += 1

    #output confusion matrix
    percent = float(right) / float(len(samples)) * 100.0
    print "Samples correctly classified: " + str(percent) + "%"
    print "Profit obtained: $" + str(profit)
    print "Confusion Matrix: (rows = output classes, columns = correct classes)"
    print "\t1\t2\t3\t4"
    print "\tv\tv\tv\tv\n"
    for r in range(len(confusionMatrix)):
        sys.stdout.write(str(r+1) + ">\t")
        for c in range(len(confusionMatrix[r])):
            sys.stdout.write(str(confusionMatrix[r][c]) + "\t")
        sys.stdout.write("\n")

    #reset samples to evenly distributed ones
    samples = []
    for f1 in range(GRID_SIZE+1):
        for f2 in range(GRID_SIZE+1):
            feature1 = float(f1) / float(GRID_SIZE)
            feature2 = float(f2) / float(GRID_SIZE)
            sample = [[feature1, feature2], None]
            sample[1] = evaluate(sample, inputNeurons, inputHiddenWeights, hiddenNeurons, hiddenOutputWeights, outputNeurons)
            samples += [sample]

    #make plot
    plotSamples(samples)


main();
